<?php

// The core functionality required by the CMS itself
require_once(dirname(__FILE__) . '/core.php');

// Other functionality considered useful to applications developing within the CMS
require_once(dirname(__FILE__) . '/framework.php');

?>
