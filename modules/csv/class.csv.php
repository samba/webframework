<?php

class CSVHandler{
	private $configuration = array();
	private $dataset = NULL;
	private $filehandle = NULL;
	private $closeFileHandleFunction = NULL;

	public function __construct($n = NULL, $columnHeaders = FALSE){
		if(!empty($n)){
		       	$this->configuration['filename'] = $n;
		}

		$this->configuration['columnHeaders'] = $columnHeaders;
	}

	public function & config($name = NULL){
		return $this->configuration[$name];
	}

	public function & data(){
		if(is_array($this->dataset)) return $this->dataset;

		$rowNum = 0;
		$columns = NULL;
		$this->dataset = array();

		$this->filehandle = $this->getFileHandle($this->configuration['filename'], 'read');
		if($this->filehandle === FALSE){
			user_error('Could not read CSV: ' . $this->configuration['filename']);
			return $this->dataset;
		}
		while(($row = fgetcsv($this->filehandle)) !== FALSE){
			$this->dataset[] = $row;
		}

		$this->closeFileHandle($this->filehandle);

		if($this->configuration['columnHeaders']){
			$columns = & $this->dataset[0];
			for($i = 1; $i < count($this->dataset); $i++){
				foreach(array_keys($this->dataset[$i]) as $k){
					$this->dataset[$i][$columns[$k]] = & $this->dataset[$i][$k];
					unset($this->dataset[$i][$k]);
				}
			}
		}

		return $this->dataset;
	}

	public function column($name = NULL){
		if(is_null($name)) return FALSE;
		if(is_string($name) or is_integer($name)) $k = $name;
		else return FALSE;

		$r = array();

		for($i = 1; $i < count($this->dataset); $i++){
			if(isset($this->dataset[$i][$k]))
				$r[$i] = & $this->dataset[$i][$k];
		}

		return $r;
	}
	
	public function saveFile($n = NULL){
		if(is_null($n) or !is_string($n) or empty($n)) 
			$n = & $this->configuration['filename'];

		$this->filehandle = $this->getFileHandle($n, 'write');

		foreach(array_keys($this->dataset) as $k){
			fputcsv($this->filehandle, $this->dataset[$k]);
		}

		$this->closeFileHandle($this->filehandle);
	}


	private function & getFileHandle($name, $mode){
		if(preg_match('/.gz$/', $name)) $func = array('gzopen', 'b9', 'gzclose');
		else $func = array('fopen', '+', 'fclose');
	
		$fh = NULL;

		switch($mode){
		case 'write':
			$fh = $func[0]($name, 'w' . $func[1]);
			break;
		case 'read':
		default:
			if(!is_file($name)) return FALSE;
			$fh =  $func[0]($name, 'r' . $func[1]);
		}
		
		$this->closeFileHandleFunction = $func[2];
		
		return $fh;
		
	}

	private function closeFileHandle(& $fh){
		return call_user_func($this->closeFileHandleFunction, $fh);
	}
}



?>
