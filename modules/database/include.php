<?php

global $dbConnection, $dbQuery;

$module = 'mysqli'; // name of folder containing implementation

$d = dirname(__FILE__) . '/';

$c = require($d . 'config.php'); // should return an array compatible with the args of iDatabaseConnection::__construct()
require($d . 'interface.php');	
require($d . $module . '/include.php');

// Find out if the class defined these properly.

if(!defined('dbConnection')) user_error('Constant dbConnection not properly defined.');
if(!defined('dbQuery')) user_error('Constant dbQuery not properly defined.');

// Prep global var aliases for the database types
$dbConnection = constant('dbConnection');
$dbQuery = constant('dbQuery');


// Implementation - create the global/default database connection.
$GLOBALS[iDatabaseConnection::defaultConnection] = 
	new $dbConnection($c['host'], $c['username'], $c['password'], $c['database']);


?>
