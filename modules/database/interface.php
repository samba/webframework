<?php

// Development documentation

// DO NOT CHANGE THIS FILE!
// This file controls the interaction between different parts of the database
// support system. Customizing thils file will likely break your site!

interface iDatabaseConnection{
	const defaultConnection = 'GlobalDBConnection'; // a string to identify the default in $GLOBALS

	// __construct() should simply alias connect(), like mysqli

	public function connect(	// establishes the connection
		$host = null,		// if null, assume localhost
		$username, 		// must be specified
		$password = null,	// if null, check no-password accounts
		$database = null,	// if null, default or select none
		$port = null, 		// if null, assume default
		$socket = null		// if null, assume non-socket connection
	); // returns nothing
	
	public function query(
		$string, 		// SQL query string
		$mode = null		// if null, assume sensible default
	); // returns an iDBQuery implementation
}

interface iDatabaseQuery{
	public function __construct(	
		$str, 			// SQL query string
		$mode = null, 		// if null, assume sensible default
		$connection = null	// connection to use - default if not specified
	); // returns nothing

	public function & data(		// array of the resulting data
		$mode = null		// if null, assume sensible default
	); // return multi-dim array

	public function & structure(	// array describing the data's structure
	); // return multi-dim array - format varies by db vendor

	public function error(		// indicate success/failure of query
	); // return boolean false on success, or array on failure
}

// Other database implementations should meet these specifications. If you find
// a database system that cannot comply, please submit a bug report.

/*

Implementation of these should also accompany definition of constants
	dbConnection
	dbQuery
	
... these allow us to refer to them globally, while still using classes
named appropriately, e.g. mysqliDBSession, postgresDB, etc...

Usage is thus simplified... all a user must do:
	
	global $dbQuery; 	// = constant('dbQuery');
	$q = new $dbQuery('select * from table');

*/
?>
