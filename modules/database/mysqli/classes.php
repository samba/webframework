<?php

// Alias the classes... this provides a bit of abstraction to simplify
// use of database support across other modules and such
define('dbConnection', 'mysqliDBSession');
define('dbQuery', 'mysqliDBQuery');


// This implementation does not include much caching.
// Previous implementations cached structural information extensively, and while
// this was nifty and didn't really cause problems, it's now seen as unnecessary,
// as most uses of this module will not make repeated calls for structural or
// other complex information.


class mysqliDBSession extends mysqli implements iDatabaseConnection{
	private $credentials = array();

	public function __construct(){
		$a = func_get_args();
		call_user_func_array(array($this, 'connect'), $a);
	}

	public function __destruct(){
		@ parent::close();
	}
////////////////////////////////////////////////////////////////////////////////	
/* Session serialization support */


	public function __sleep(){
		return array('credentials');
	}
	public function __wakeup(){
		call_user_func_array(array($this, 'connect'), $this->credentials);
	}

////////////////////////////////////////////////////////////////////////////////


	public function connect($host = null, $username, $password = null, $database = null, $port = null, $socket = null){
		$this->credentials = func_get_args();
		call_user_func_array(array(&$this, 'parent::connect'), $this->credentials);
	}
	
	public function query($str, $mode = null){
		if(!in_array($mode, array(MYSQLI_STORE_RESULT, MYSQLI_USE_RESULT))){
			user_error("Invalid Query Mode; defaulting to MYSQLI_STORE_RESULT", E_USER_WARNING);
			$mode = MYSQLI_STORE_RESULT;
		}
		
		return parent::query($str, $mode);
	}	

} // end mysqliDBSession class






class mysqliDBQuery implements iDatabaseQuery{
	private $query_string = null;
	private $query_result = null;
	private $errorDetail = false;
	private $connection = null;
	
	public function __construct($str, $mode = null, $connection = null){ 
		global $db_connection;
		// Get a valid DB connection
		if(is_null($connection) or !($connection instanceof iDatabase))
			$this->connection = & $GLOBALS[iDatabaseConnection::defaultConnection];
		else $this->connection = & $connection;
		$this->query_string = & $str;
		// Run the query, store error information
		if(!($this->query_result = & $this->connection->query($str, $mode))){
			$this->errorDetail = array(
				'query'		=> & $this->query_string, // provide the query for debugging
				'description'	=> $this->connection->error,
				'number'	=> $this->connection->errno,
				'state'		=> $this->connection->sqlstate
			);
		}else $this->errorDetail = false;
	}
	public function __destruct(){
		if(is_object($this->query_result)) $this->query_result->close();
	}
////////////////////////////////////////////////////////////////////////////////	
/* Session serialization support */


	public function __sleep(){
		return array('query_string');
	}
	public function __wakeup(){
		$this->__construct($this->query_string);
	}
////////////////////////////////////////////////////////////////////////////////

	
	public function error(){
		return $this->errorDetail;
	}

	public function & data($mode = null){
		$rows = array();
		if(!in_array($mode, array(MYSQLI_NUM, MYSQLI_ASSOC, MYSQLI_BOTH))){
			user_error('Invalid Result Parsing Mode; defaulting to MYSQLI_ASSOC', E_USER_WARNING);
			$mode = MYSQLI_ASSOC;
		}
		if(!$this->error()) while($rows[] = $this->query_result->fetch_array($mode)){
			// do nothing, it's already added to the array
			// although we might link to structural data or something...
		}
		return $rows;
	}
	
////// Begin structural analysis features...
	
	public function & structure(){
		$s = NULL;
		if(!$this->error()){
			$s = $this->query_result->fetch_fields();
			foreach(array_keys($s) as $k){
				// Get the object and re-map it by name, rather than numeric index
				$fieldObj = & $s[$k];
				$s[$fieldObj->name] = & $fieldObj;
				unset($s[$k]);
				
				// Extended structural analysis
				$fieldObj->extend = array();
				$r = $this->structGetFieldDetails($fieldObj);
				// verify field type
				$fieldObj->type = $this->struct_MySQLi_type_bug($fieldObj, $r);
				$fieldObj->extend['options'] = $this->structFieldOptions($fieldObj, $r);
				$fieldObj->extend['type'] = $this->structFieldType($fieldObj);
				$fieldObj->extend['flags'] = $this->structFieldFlags($fieldObj);
			}
		}
		
		return $s;
	}
	
	private function & structGetFieldDetails(& $field){
		// Ask the database for a different view of structural info
		$q = sprintf("SHOW FIELDS FROM `%s` LIKE '%s'",
			$field->orgtable,
			$field->orgname
		);
		$result = $this->connection->query($q);
		$detailRow = $result->fetch_assoc(); $result->close();
		return $detailRow;
	}
	
	private function struct_MySQLi_type_bug(& $field, & $detailRow){
		if(!isset($field->type) or !is_array($detailRow)) return FALSE;
		// Addresses a bug where MySQLi confuses SET and ENUM types
		// as MYSQLI_TYPE_STRING
		if(preg_match('/^enum/', $detailRow['Type'])) return MYSQLI_TYPE_ENUM;
		elseif(preg_match('/^set/', $detailRow['Type'])) return MYSQLI_TYPE_SET;
		else return $field->type;
		// See also:
		// http://svn.mysql.com/svnpublic/mysqli_converter/MySQLConverterTool/UnitTests/Converter/TestCode/field_type001.php
	}
	
	private function structFieldOptions(& $field, & $detailRow){
		if(!isset($field->type) or !is_array($detailRow)) return FALSE;
		// currently handles only native optionals... 
		// foreign key resolution will be added later
		if(!in_array($field->type, array(MYSQLI_TYPE_ENUM, MYSQLI_TYPE_SET)))
			return FALSE;		
		
		$options = array();
		$n = preg_match_all('/\'(.*)\'/U', $detailRow['Type'], $options);
		
		return $options[1]; // just the seleted parts from the regex
	}
	private function & structFieldType(& $field){		
		$r = false;
		if(isset($field->type)){
			if(!isset(mysqliDBQuery::$mysqliFieldTypes[$field->type]))
				user_error('Unregistered MySQLi Field Type constant: ' . $field->type, E_USER_WARNING);
			$r = mysqliDBQuery::$mysqliFieldTypes[$field->type];
			if(isset($field->options) and is_array($field->options)){
				// Set the form handler if the field has options
				if(in_array($r[0], $array('text', 'textarea'))) $type[0] = 'checkbox';
			}
		}
		return $r;
	}

	private function & structFieldFlags(& $field){
		if(!isset($field->flags)) return false;

		// Translate flags into human readable text
		
		$r = array($field->flags); // include the original as first element
		foreach(array_keys(mysqliDBQuery::$mysqliFieldFlags) as $k){
			if($field->flags & $k) $r[] = mysqliDBQuery::$mysqliFieldFlags[$k];
		}
		return $r; // an array of strings
	}
	

/* /////////////////////////////////////////////////////////////////////////////
MYSQLI Structural Information Abstraction makes it human readable and more useful.
----------------------------------------------------------------------------- */
	public static $mysqliFieldTypes = array(
	        // Mask the order of values in each entry's array...
	        'mask'                          => array('form' => 0,   'desc' => 1),
	        
	        // MySQL Type Specifications...
	        MYSQLI_TYPE_DECIMAL             => array('text',        'Decimal'),
	        MYSQLI_TYPE_NEWDECIMAL          => array('text',        'Decimal (precision)'),
	        MYSQLI_TYPE_BIT                 => array('checkbox',    'Bit'),
	        MYSQLI_TYPE_TINY                => array('text',        'Tiny Integer'),
	        MYSQLI_TYPE_SHORT               => array('text',        'Integer (short)'),
	        MYSQLI_TYPE_LONG                => array('text',        'Integer (long)'),
	        MYSQLI_TYPE_FLOAT               => array('text',        'Float'),
	        MYSQLI_TYPE_DOUBLE              => array('text',        'Double'),
	        MYSQLI_TYPE_NULL                => array('text',        'Default NULL'),
	        MYSQLI_TYPE_LONGLONG            => array('text',        'Big Integer'),
	        MYSQLI_TYPE_INT24               => array('text',        'Medium Integer'),
	        
	        MYSQLI_TYPE_DATE                => array('text',        'Date'),
	        MYSQLI_TYPE_TIME                => array('text',        'Time'),
	        MYSQLI_TYPE_DATETIME            => array('text',        'Date/Time'),
	        MYSQLI_TYPE_TIMESTAMP		=> array('text',	'Timestamp'),
	        MYSQLI_TYPE_YEAR                => array('text',        'Year'),
	        MYSQLI_TYPE_NEWDATE             => array('text',        'Date (new)'),
	        
	        MYSQLI_TYPE_ENUM                => array('radio',       'Enumerated'),
	        MYSQLI_TYPE_SET                 => array('checkbox',    'Set'),
	        MYSQLI_TYPE_TINY_BLOB           => array('textarea',    'Tiny Blob'),
	        MYSQLI_TYPE_MEDIUM_BLOB         => array('textarea',    'Medium Blob'),
	        MYSQLI_TYPE_LONG_BLOB           => array('textarea',    'Long Blob'),
	        MYSQLI_TYPE_BLOB                => array('textarea',    'Blob'),
	        MYSQLI_TYPE_VAR_STRING          => array('textarea',    'Variable Length String (varchar)'),
	        MYSQLI_TYPE_STRING              => array('textarea',    'String (char)'),
	        MYSQLI_TYPE_GEOMETRY            => array('textarea',    'Geometry')
	);
	public static $mysqliFieldFlags = array(
	        MYSQLI_PRI_KEY_FLAG             => 'Primary Key',	// part of primary index
	        MYSQLI_UNIQUE_KEY_FLAG          => 'Unique Key',	// part of a unique index
	        MYSQLI_MULTIPLE_KEY_FLAG        => 'Multiple Key',	// part of an index (not specific)
	        MYSQLI_PART_KEY_FLAG            => 'Partial Key',	// part of a multi-index
	        MYSQLI_NUM_FLAG                 => 'Numeric',
	        MYSQLI_UNSIGNED_FLAG            => 'Unsigned', 
	        MYSQLI_ZEROFILL_FLAG            => 'Zero-fill',
	        MYSQLI_AUTO_INCREMENT_FLAG      => 'Auto-increment',
	        MYSQLI_TIMESTAMP_FLAG           => 'Timestamp',		// is a timestamp of some sort
	        MYSQLI_BLOB_FLAG                => 'Blob',
	        MYSQLI_SET_FLAG                 => 'Set',
	        MYSQLI_GROUP_FLAG               => 'Group',
	        MYSQLI_NOT_NULL_FLAG            => 'NULL not allowed'
	);

/////////// END MYSQLI Structural Information Abstraction //////////////////////

} // end mysqliDBQuery class


?>
