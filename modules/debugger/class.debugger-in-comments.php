<?php

/* Sam's "wait" debugger.

Catches all errors and exceptions and posts them to the browser via the
destructor - should be the last thing happening.

This is not yet considered complete.

*/

error_reporting(E_ALL);
define('CMS_DEBUGGER_CLASS', 'wait_debugger');

class wait_debugger implements iDebugger{
	private	$catch	= array();
	private $handle	= array();
	
	const	GlobalReference	= '././GlobalDebugger././';
	
	public function __construct(){
		set_error_handler(array(& $this, 'error'));
		set_exception_handler(array(& $this, 'except'));
	}
	public function	__destruct(){
		restore_error_handler();
		restore_exception_handler();
		echo "<!-- DEBUGGING\n\n";
		print_r($this->catch);
		echo "\n-->";
	}
////////////////////////////////////////////////////////////////////////////////	
// Error handlers

	public	function	error($n, $s, $f, $l, $c){
		$e = array(
			'error'	=> array(
				'errno'	=> $n, 
				'desc'	=> $s,
				'file'	=> $f,
				'line'	=> $l,
				'ctext'	=> $c
			),
			'backtrace'	=> debug_backtrace()
		);
		
		$this->catch[] = $e;
	}
	public	function	except(& $e){
		$e = array(
			'exception'	=> $e,
			'backtrace'	=> debug_backtrace()
		);
		$this->catch[] = $e;
	}
	
	private	function	add_backtrace(& $e){
		$e[] = debug_backtrace();
	}

////////////////////////////////////////////////////////////////////////////////
// Session integration
	
	public	function	__wakeup()	{ return NULL; }
	public	function	__sleep()	{ return NULL; } 
}
?>
