<?php

/*

Catch all errors and exceptions, store them in session, and present a link for the user to view the error reports.

This class is more or less complete. Its use, however, requires an additional viewing mechanism. 

I will implement a display module using AJAX to monitor the $_SESSION independently.

*/
error_reporting(E_ALL); // show me everything.
session_start();


define('CMS_DEBUGGER_CLASS', 'sessionized_debugger');

class sessionized_debugger implements iDebugger{
	private	$catch	= array();
	
	const	GlobalReference	= '././GlobalDebugger././';
	
	public function __construct(){
		set_error_handler(array(& $this, 'error'));
		set_exception_handler(array(& $this, 'except'));
	}
	public function	__destruct(){
		restore_error_handler();
		restore_exception_handler();

		if(count($this->catch)){ // there are errors to report.
			// cache the report(s)
			$stamp = time();
			if(!isset($_SESSION['debugger']) or !is_array($_SESSION['debugger'])) 
				$_SESSION['debugger'] = array();
			$_SESSION['debugger'][$stamp] = & $this->catch;
	
			// present link.
		}
		// else do nothing.
	
	}
////////////////////////////////////////////////////////////////////////////////	
// Error handlers

	public	function	error($n, $s, $f, $l, $c){
		$b = debug_backtrace();
		array_shift($b);
		$e = array(
			'error'	=> array(
				'errno'	=> $n, 
				'desc'	=> $s,
				'file'	=> $f,
				'line'	=> $l,
				'ctext'	=> $c,
				'btrace'=> $b
			),
//			'summary' 	=> 'err ' . $n . ' @ ' . basename($f) . ':' . $l,
		);
		
		$this->catch[] = $e;
	}
	public	function	except(& $e){
		$e = array(
			'exception'	=> $e,
//			'summary'	=> 'exc ' . $e->getCode() . ' @ ' . basename($e->getFile()) . ':' . $e->getLine(),
		);
		$this->catch[] = $e;
	}

////////////////////////////////////////////////////////////////////////////////
// Session integration
	
	public	function	__wakeup()	{ return NULL; }
	public	function	__sleep()	{ return array(); } 
}
?>
