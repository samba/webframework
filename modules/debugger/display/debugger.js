/* Debugging Display Tool.
 * Functionality:
 * 	AJAX request of debugging information
 * 		List instances saved in session
 * 		List errors in an instance
 * 		List details of an error
 * 	DHTML interface behaviors
 * 		Menu-ized debugging structures
 */


var PHPDebugger = {
	// get instances
	loadInstances: function(){
		$.get('', {m: 0}, function(data, txtStatus){
			$('ul#instance-list').html(data);
		});
	},

	// get errors
	loadErrors: function(el){ 
		$.get(el.href, null, function(data, txtStatus){
			$('ul#error-list').html(data);
		});
		return false; 
	},
	
	// get error details
	loadDetails: function(el){
		$.get(el.href, null, function(data, txtStatus){
			$('ul#error-details-list').html(data);
		});
		return false; 
	}
}

$(document).ready(function(){
	PHPDebugger.loadInstances();	
});
