<?php

/*	debugging interface.
 *	1: serve the shell HTML/JS to the browser (no input)
 *	2: serve the list of instances in session (m defined, numeric)
 *	3: serve the list of errors of the specified instance (i is valid index)
 *	4: serve the details of the specified error (i and e are both valid index) 
 */

// We get our error records from session, so we'd better start it...
session_start();

$errortypes = array (
 E_ERROR		=> 'Error',
 E_WARNING		=> 'Warning',
 E_PARSE 		=> 'Parsing Error',
 E_NOTICE		=> 'Notice',
 E_CORE_ERROR		=> 'Core Error',
 E_CORE_WARNING		=> 'Core Warning',
 E_COMPILE_ERROR 	=> 'Compile Error',
 E_COMPILE_WARNING 	=> 'Compile Warning',
 E_USER_ERROR 		=> 'User Error',
 E_USER_WARNING 	=> 'User Warning',
 E_USER_NOTICE 		=> 'User Notice',
 E_STRICT 		=> 'Runtime Notice',
 E_RECOVERABLE_ERROR 	=> 'Recoverable (Catchable) Fatal Error'
);






if(!isset($_REQUEST['m']) and !isset($_REQUEST['i'])){
	readfile('shell.html');
}elseif(!isset($_REQUEST['i']) and isset($_REQUEST['m']) and is_numeric($_REQUEST['m'])){
	// send list of instances
	$timestamps = array_keys($_SESSION['debugger']);
	$instance_format = "<li><a href=\"%s?m=&i=%s\" onclick=\"%s\">%s [%u]</a></li>";
	foreach($timestamps as $t){
		printf($instance_format, 
			$_SERVER['PHP_SELF'],
			$t,
			'return PHPDebugger.loadErrors(this)',
			date('Y M d H:i:s', $t),
			count($_SESSION['debugger'][$t])
		);
	}

}elseif(isset($_REQUEST['i']) and isset($_SESSION['debugger'][$_REQUEST['i']])){
	
	if(!isset($_REQUEST['e']) or !isset($_SESSION['debugger'][$_REQUEST['i']][$_REQUEST['e']])){
		// send list of errors in the instance
		$error_format = "<li><a href=\"%s?m=&i=%s&e=%s\" onclick=\"%s\">%s</a></li>";
		foreach(array_keys($_SESSION['debugger'][$_REQUEST['i']]) as $e){
			$error = & $_SESSION['debugger'][$_REQUEST['i']][$e];

			printf($error_format,
				$_SERVER['PHP_SELF'],
				$_REQUEST['i'],
				$e,
				'return PHPDebugger.loadDetails(this)',
				makeSummary($error)	
			);	
		}

	}else{
		// send error details
		$error = & $_SESSION['debugger'][$_REQUEST['i']][$_REQUEST['e']];
			
		echo "<pre>";
		print_r($error);
		echo "</pre>";


	}
}else print_r($_REQUEST);



function makeSummary(& $e){
	global $errortypes;
	$format = "%s at %s:%u";
	if(isset($e['error'])){	
		return sprintf($format, $errortypes[$e['error']['errno']], basename($e['error']['file']), $e['error']['line']);
	}elseif(isset($e['exception'])){
		return sprintf($format, get_class($e['exception']), basename($e['exception']->getFile()), $e['exception']->getLine());
	}else return "Unknown error structure.";
}

function makeDetail(& $e){

}

?>
