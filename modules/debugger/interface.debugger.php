<?php
/* Sam's Debugger interface
Lets me create new debuggers and ensure that they'll be useful to the rest of
the system.
*/


interface iDebugger{
	/* Standard PHP object control functions	*/
	public	function	__construct();
	public	function	__destruct();
//	public	function	__get($n);
//	public	function	__set($n, $v);
//	public	function	__call($n, $a);
	public	function	__wakeup();
	public	function	__sleep();
	
	
	/* Error handlers	*/
	public	function	error($n, $s, $f, $l, $c);
	public	function	except(& $e);
	
	/*
	The rest of a debugger's functions are subject to the purpose of each 
	implementation. Errors can be cached until the destructor is called, or
	can be dumped directly, or handled via other means. 
	*/
}


?>
