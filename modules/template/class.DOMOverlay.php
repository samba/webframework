<?php

/*
30/April/2008. Trying something new here...
*/

class DOMOverlay extends DOMDocument{
	private $template	= array(	// contains all the custom data structures
		'overlays'	=> NULL,	// node list of all <link rel="template">
		'injections'	=> NULL,	// array of files to import on IDs
	);
	private 	$index	= -1;
	private		$xpath	= NULL;
	private		$head, $body;
	
	public function 	__construct($version = '1.0', $encoding = 'iso-8859-1', $strict = FALSE){
		$this->debug('Constructing.');
		
		// Sensible defaults
		$this->validateOnParse 		= TRUE;
		$this->formatOutput		= TRUE;
		$this->preserveWhiteSpce	= TRUE;
		$this->strictErrorChecking	= (bool) $strict;
		
		// call parent constructor
		parent::__construct($version, $encoding);
		
	}
	public function 	__destruct(){}

	
/* Disabled: let these have their default functionality for now...
	public	function	__get($n){}
	public	function	__set($n, $v){}
	public	function	__call($n, $a){}
	public	function	__wakeup(){}
	public	function	__sleep(){}
*/

////////////////////////////////////////////////////////////////////////////////
// Interfaces

	public function		debug($s, $t = E_USER_NOTICE){
		return user_error($s, $t);
	}

	public function &	inject($id, $file){
		// TODO return the node where the file was injected.
	}	

	public function &	overlay(& $template){
		// I am the document, I define subsections of the template. 
		
		$x = new DOMXPath($template);
		
		$this->mergeHead($template, $x);
		$this->mergeBody($template, $x);
		
		return $this;
	}



////////////////////////////////////////////////////////////////////////////////
// Output processors

	public function		saveHTML(){
		// This originally contained a wrapper to dump internal debugging.
		// Debugging is now handled by an external error handler.
		return parent::saveHTML();
	}

	public function		__toString(){
		return $this->saveHTML();
	}	

////////////////////////////////////////////////////////////////////////////////
// Input processors

	public function		loadHTML($s){
		$r = parent::loadHTML($s);
		unset($this->xpath); $this->xpath = new DOMXPath($this);
		$this->setupNavigators();
		return $r;
	}
	public function		loadHTMLFile($f){
		if(!is_file($f)) return false;
		$r = parent::loadHTMLFile($f);
		$this->setupNavigators();
		return $r;
	}
	
	private function	setupNavigators(){
		unset($this->xpath); $this->xpath = new DOMXPath($this);
		$this->head = $this->xpath->query('/html/head')->item(0);
		$this->body = $this->xpath->query('/html/body')->item(0);
	}
	
////////////////////////////////////////////////////////////////////////////////
// Basic DOM processors	
	
	// switch two nodes from two documents
	private function	swapNodes(& $one, & $two){
		$this->debug('orchestrating node swap');
		
		$o = $one;
		$one = $two->ownerDocument->importNode($one, TRUE);
		$two = $two->parentNode->replaceChild($one, $two);
		$two = $o->ownerDocument->importNode($two, TRUE);
		$one = $o->parentNode->replaceChild($two, $o);
		return TRUE;
	}
	
////////////////////////////////////////////////////////////////////////////////
// DOM Integrators

	private function	mergeHead(& $t, & $tx){
		$this->debug('merging headers');
		$this->mergeTitle($tx);
		$this->mergeMeta($tx);
		
		// everything else. TODO: see if we can clean this up
		
		foreach($tx->query('/html/head/child::*') as $n){
			
			$n = $this->importNode($n, TRUE);
			if($n->hasAttribute('id')){
				$s = $this->xpath->query('/html/head//*[@id="'.$n->getAttribute('id').'"]');
				if(!$s->length){
					$n = $this->head->appendChild($n);
				}else $n = $this->head->appendChild($n);
			}else{
				$n = $this->head->appendChild($n);
			}
		}
	}
	
	private function	mergeBody(& $t, & $tx){
		$this->debug('merging body');
		$t_body = $tx->query('/html/body')->item(0);
		if(!$t_body) return false; // just in case there is no body in the overlay, we won't panic

		// merge bodies TODO: clean up this logic a bit
		foreach($this->xpath->query('/html/body/child::*') as $n){
			$n = $n->parentNode->removeChild($n);
			$n = $t->importNode($n, TRUE);
			if($n->hasAttribute('id')){
				$s = $tx->query('/html/body//*[@id="'.$n->getAttribute('id').'"]');
				if($s->length){
					$p = $s->item(0)->parentNode; $o = $s->item(0)->nextSibling;
					$x = $p->removeChild($s->item(0));
					$n = $p->insertBefore($n, $o);
				}else  
					$n = $t_body->appendChild($n);
			}else{
				$n = $t_body->appendChild($n);
			}
		}
		// adopt current template content
		foreach($tx->query('/html/body/child::*') as $n){
			$this->body->appendChild($this->importNode($n, TRUE));
		}
		
	}

	private	function	mergeTitle(& $tx){
		/* This is a bit complex.
		First I poll the title(s) from the document, and then the 
		template. I merge them using `sprintf`. The first iteration is 
		just a direct insertion, hence the '%s' start value. In effect, 
		once a  title is encountered that does NOT contain the string 
		'%s', it stops performing the merge. The page title is applied 
		first, so template titles will only be merged if the document 
		title contains '%s', and so forth in sequence of template
		application. */

		$titles = array(
			$this->title(),
			$tx->query('/html/head//title')
		);

		$output = '%s';
		foreach($titles as $t){
			foreach($t as $n){
				$output = sprintf($output, $n->textContent);
				// Remove the title to avoid duplication
				$n = $n->parentNode->removeChild($n);
			}
		}

		// Insert the new title
		$this->head->insertBefore($this->createElement('title', $output), $this->head->firstChild);
	}

	private	function	mergeMeta(& $tx){
		// for the standard META in the template
		foreach(array('name', 'http-equiv') as $t){
			foreach($this->meta($t, NULL, NULL, $tx) as $m){
				// search for similar nodes
				$s = $this->meta($t, $m->getAttribute($t));
				if($s->length){ // does a similar node exist?
					// if document specifies inclusion of template meta, make it happen
					$s->item(0)->setAttribute('content', 
						sprintf(
							$s->item(0)->getAttribute('content'),
							$m->getAttribute('content')
						)
					);
				//	$m = $m->parentNode->removeChild($m);
				}else{ // apparently not; just insert it.
					$b = $this->importNode($m, TRUE);
					$this->head->appendChild($b);
				}
				if($m->parentNode) $m = $m->parentNode->removeChild($m);
			}
		}
	}

	private function & swapOnID(& $d, & $t, $path = NULL){
		$p = array(new DOMXPath($d), new DOMXPath($t));
		$c = array(
			(is_null($path))? $d : $p[0]->query($path)->item(0),
			(is_null($path))? $t : $p[1]->query($path)->item(0)
		);
		// get all elements in template with an id
		foreach($p[1]->query('//*[@id]', $c[1]) as $i){
			$f = FALSE;
			$id = $p[1]->evaluate('string(@id)', $i);
			// get all matching elements from document
			$n = $p[0]->query(sprintf('//*[@id="%s"]', $id), $c[0]); 
				
			if(!$n->length) continue;
			else
				$this->swapNodes($i, $n->item(0));
		}
		unset($p);
		return $this;
	}




////////////////////////////////////////////////////////////////////////////////
// DOM Search Abstractors

	public function	 	title($x = NULL){
		if(!($x instanceOf DOMXPath)) $x = & $this->xpath;
		$this->debug('getting title nodes');
		return $x->query('/html/head//title');
	}
	
	public function 	meta($m = NULL, $v = NULL, $c = NULL, $x = NULL){
		if(!($x instanceOf DOMXPath)) $x = & $this->xpath;
		$q = '/html/head//meta';
		if(!is_null($m) and is_null($v)){
			$q .= '[@%1$s]';
		}elseif(!is_null($m) and !is_null($v)){
			$q .= '[@%1$s="%2$s"]';
		}
		if(!is_null($c))
			$q .= '[@content="%3$s"]';
		
		$q = sprintf($q, $m, $v, $c);
		$this->debug('getting meta nodes: ' . $q);

		return $x->query($q);
	}
	
	public function  	templates($x = NULL){
		if(!($x instanceOf DOMXPath)) $x = & $this->xpath;
		$this->debug('getting template links');
		return $x->query('/html/head//link[@rel="template"]');
	}
}

?>
