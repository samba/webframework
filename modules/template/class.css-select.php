<?php

// Facility to allow CSS selectors to be used on DOM docs in PHP

/* CSS parsing notes...
	Using something like BNF... (using {} instead of <>)
		{selector} := {node}
			| {selector} {node}		// general descendant
			| {selector} > {node}		// child
			| {selector} + {node}		// adjacent
			| {selector} , {selector}	// combined
		
		{node}	:= {tag} | * | {blank}
			| {node}.{class}
			| {node}#{id}
			| {node}:{pseudo}
			| {node}[{attr}]
			
		{attr}	:= {word}
			| {word}={word}
			| {word}~={word}
			| {word}|={word}
			
		{class} := {word}
		{id}	:= {word}
		{pseudo}:= {word}
		{word}	:= (an alpha-numeric unbroken string)
		{blank} := (empty space)
		

*/

class DOMDocumentCSS extends DOMDocument{
	public	$css		= array(
		'xpath_obj'	=> null,
		'selection'	=> array()
	);
	
	public function & select($s){
		// for each selector in the list (comma-separated)
		foreach($this->CSSparseSelector($s) as $s){
			$s = $this->CSSgetXPaths($s);
			$this->css['xpath_obj'] = new DOMXPath($this);
			$this->css['selection'][] = $this->css['xpath_obj']->query($s); 
		}
		return ($this->css['selection']); // should be an array of DOMNodeLists
	}

	
	private function	CSSgetXPaths(& $s){
		// translate CSS into XPaths.
		
		$replacements = array( /* ORDER MATTERS!!! */
		
			// class (remapped as attribute)
			'/\.([\w]+)/'			=> '[class~=\\1]',
			// pseudo-class (remapped as attribute)
			'/:([-\w]+)(\(([\w]+)\))/'	=> '[xml:\\1|=\\3]',
			
			// attributes
			'/\[([\w]+)\]/'			=> '[@\\1]',
			'/\[([\w]+)=([\w]+)\]/'		=> '[@\\1="\\2"]',
			'/\[([\w]+)~=([\w]+)\]/'	=> '[contains(concat(" ",@\\1," "),concat(" ","\\2"," "))]',
			'/\[([:\w]+)\|=([\w]+)\]/'	=> '[@\\1="\\2" or starts-with(@\\1,concat("\\2","-"))]',
			
			// id
			'/#([\w]+)/'			=> '[id("\\1")]',
			
			// pseudo-element
			'/(.*):first-child/'		=> '*[1]/self::\\1'
		);
		
		
		for($i = 0; $i < count($s); $i++){
	
			$s[$i] = preg_replace(array_keys($replacements), $replacements, $s[$i]);
		
			switch($s[$i]){
				case '>': $s[$i] = '/'; break; // direct descendence
				case '+': $s[$i] = '/following-sibling::*[1]/self::'; break; // adjacency
				case ''	: $s[$i] = '//'; break; // general descendence
				default:
					// do nothing.
			}
			
		}
	
		return '//' . implode('', $s);
	}
	

	
	// split a selector list (by comma, and then by combinator)
	private function	CSSparseSelector(& $s){
		$selectors = array_map('trim', explode(',', $s));
		for($i = 0; $i < count($selectors); $i++){
			$selectors[$i] = preg_split('/([+>\s]+)/', $selectors[$i], -1, PREG_SPLIT_DELIM_CAPTURE);
			$selectors[$i] = array_map('trim', $selectors[$i]);
		}
		return $selectors;
	}

}
?>
