<?php

// Revamped template parser.
// For simplicity in porting this to other XML systems, 
// I'm using XPath wherever possible. 

class template{

	const GlobalReference = '././GlobalTemplate././';
	private $xmlDocument	= null;
	private $xmlTemplate	= null;
	private	$replacements	= array(
		'before'	=> array(
			'preg_replace'	=> array(),
			'str_replace'	=> array()
		),
		'after'		=> array(
			'preg_replace'	=> array(),
			'str_replace'	=> array()
		)
	);
	private	$injections	= array();
	
	private static $count	= 0;
	private	$id		= 'invalid';
	
	public function __construct(){
		/* don't do much here.
		This version is entirely destructor-oriented. The idea is that 
		it leaves-alone everything that happens before the page is 
		finished, and then manipulates the output to fit the specified 
		templates.
		*/
		
		// Handle {en|de}coding HTML entities in <a href="{URL}">
		// ... this prevents the parsers from complaining on valid URLs
		$pattern = "/href=\"(.*)\"/Uie";
		$encode = "'href=\"'.htmlentities('\\1').'\"'";
		$decode = "'href=\"'.html_entity_decode('\\1').'\"'";	
		$this->replace($pattern, $encode, 'before');
		$this->replace($pattern, $decode, 'after');
	}
	
	public function __destruct(){
	
		
		$input = ob_get_clean();
		ob_start();
		
		// run 'before' replacements	
		$input = $this->do_replace($input, 'before', 'str_replace');
		$input = $this->do_replace($input, 'before', 'preg_replace');
		
		$this->xmlDocument = new DOMOverlay();
		$this->xmlDocument->loadHTML($input); unset($input);
		
		foreach($this->xmlDocument->templates() as $t){
		// Overlay all specified files.
			$f = constant('CMS_ROOT') .$t->getAttribute('href');
			if(is_file($f) and is_readable($f)){
				$this->xmlTemplate = new DOMOverlay();
				if(!$this->xmlTemplate->loadHTMLFile($f))
					user_error('Error loading template ($f).');
				else{
					user_error('Loading template: ' . $f, E_USER_NOTICE);
				
					$this->xmlDocument = $this->xmlDocument->overlay($this->xmlTemplate);
				
				}
			}else{
				user_error('Could not load template: ' . $f, E_USER_WARNING);
			}
		}
		
		$x = new DOMXPath($this->xmlDocument);
		foreach($x->query('/html//body') as $b){
			foreach($b->childNodes as $n){
				if($n->nodeType == XML_TEXT_NODE){
					$n->nodeValue = '';
				}else break;
			}
		}
		
		$output = $this->xmlDocument->saveHTML();
					
		// run 'after' replacements
		$output = $this->do_replace($output, 'after', 'preg_replace');
		$output = $this->do_replace($output, 'after', 'str_replace');
	
		// print the results.
		echo $output;
		unset($output);
		
	}
	
/* Disabled: let these have their default functionality for now...
	public	function	__get($n){}
	public	function	__set($n, $v){}
	public	function	__call($n, $a){}
	public	function	__wakeup(){}
	public	function	__sleep(){}
 */
	public function & replace($search, $replace, $timing = 'after'){
		$type = (preg_match('/\/.*\//', $search)) ? 'preg_replace' : 'str_replace';
		$this->replacements[$timing][$type][$search] = & $replace;
		return $this->replacements[$timing][$type][$search];
	}
	
	private function & do_replace(& $buffer, $timing, $type){
		
		$buffer = $type(
			array_keys($this->replacements[$timing][$type]),
			$this->replacements[$timing][$type],
			$buffer
			);
		return $buffer;
	}







/////////////////////// Supporting Functions ///////////////////////////////////
	
	// searches a document for template links and returns an array of template files.
	public static function & getTemplateReferences(& $x){
		// returns either an array of matching LINK elements or FALSE
		$templates = array();
	
		if(!($x instanceof DOMDocument)) user_error(__FUNCTION__ . ' requires argument of type DOMDocument');
	
		$p = new DOMXPath($x);
		$l = $p->query('/html/head/link[@rel="template"]');
		
		for($i = 0; $i < $l->length; $i++){
			$templates[] = $l->item($i)->getAttribute('href');
		}
		
		return (count($templates)) ? $templates : FALSE;
	}
	
	// Generates an XML document the way I like.
	public static function & newDocument($strict = FALSE, $enableCSS = FALSE){	
		if(!(bool) $enableCSS)	$n = new DOMDocument();
		else{
		 	if(!class_exists('DOMDocumentCSS'))
		 		user_error('CSS extension not enabled.', E_USER_ERROR);
		 	$n = new DOMDocumentCSS();
		 }
		// some sensible defaults.
		$n->validateOnParse = TRUE;
		$n->formatOutput = TRUE;
		$n->preserveWhiteSpce = FALSE;
		$n->strictErrorChecking = (bool) $strict;
		return $n;
	}
	
}
?>
