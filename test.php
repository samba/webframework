<?php

// This is a test script for all CMS modules.
$n = require('include.php');



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head profile="modules/template/profile.html">
    <meta name="generator"
          content=
           "HTML Tidy for Linux (vers 6 November 2007), see www.w3.org" />
          
    <title>
      CMS Test page [%s]
    </title>
    <meta name="keywords"
          content="%s,example keyword" />
    <meta name="author"
          content="taxi%s" />
    <link rel="stylesheet"
          href="briesemeister_style.css"
           type="text/css" />
    <link rel="template"
          href="site/template/briesemeister-core.html" />
    <link rel="template"
          href="site/template/briesemeister-secondary.html" />
     <style type="text/css">
/*<![CDATA[*/
    li.c1 {list-style: none; display: inline}
    /*]]>*/
    </style>
  </head>
  <body>
    <div id="siteID">
			<h2>
				Site Heading 2
      </h2>
      <h1>
        Template Version 2
      </h1>
    </div>
    <div id="toolbar"></div>
    <div id="content">
      <div id="nav">
        <div id="featured">
          <ul>
            <li>
              <a href="Personnel.html">Contact Us</a>
            </li>

          </ul>
        </div>
        <ul>
          <li>
            <a href="Resources.html"
                class="parentpath">Resources</a>
          </li>
          <li>
            <a href="Training.html"
                class="parentpath">Training</a>
          </li>
        </ul>
      </div>
      <div id="main">
      </div>
      <div id="secondary">
      </div>
      <div id="additional"></div>
      <div id="localfooter">
      </div>
    </div>
  </body>
</html>


<?php
// clear the existing errors.
if(isset($_REQUEST['clear-errors'])) $_SESSION['debugger'] = array();
user_error('Testing the debugger - soft error');
throwThatException();


function throwThatException(){
	throw new Exception('Testing the debugger - exception');
}
?>
